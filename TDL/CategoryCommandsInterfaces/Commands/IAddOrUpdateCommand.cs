﻿using CategoryInterfaces.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryInterfaces.Commands
{
    public interface IAddOrUpdateCommand
    {
        void AddOrUpdate(ICategoryDto category);
    }
}
