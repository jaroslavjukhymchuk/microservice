﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryInterfaces
{
    public interface IDeleteCommand
    {
        void Delete(string id);
    }
}
