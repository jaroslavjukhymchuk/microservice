﻿using CategoryInterfaces.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryInterfaces.Queries
{
    public interface IGetCategoruQuery
    {
        IEnumerable<ICategoryDto> GetCategories();
    }
}
