﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryInterfaces.Dtos
{
    public interface ICategoryDto
    {
         string Name { get; set; }

        string Id { get; set; }

        Guid IdUser { get; set; }
    }
}
