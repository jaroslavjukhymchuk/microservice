﻿using Incoding.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryPersistences.Entities
{
    public class Category : IncEntityBase
    {
        public virtual string Name { get; set; }

        public virtual string Id { get; set; }

        public virtual Guid IdUser { get; set; }

        #region Nested Classes

        public class Map : NHibernateEntityMap<Category>
        {

            protected Map()
            {
                IdGenerateByGuid(r => r.Id);
                MapEscaping(r => r.Name);
                MapEscaping(r => r.IdUser);
            }
            
        }
        #endregion
    }
}
