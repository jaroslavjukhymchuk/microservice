﻿using CategoryCommands.ExecuteCommands;
using CategoryInterfaces;
using CategoryInterfaces.Commands;
using CategoryInterfaces.Queries;
using CategoryQueries.ExecuteQueries;
using Ninject.Modules;

namespace CategoryIoC
{
    public class CategoryNinjectModule : NinjectModule
    {
        public override void Load()
        {
              Bind(typeof(IAddOrUpdateCommand)).To(typeof(ExecuteAddOrUpdateCommand));
              Bind(typeof(IDeleteCommand)).To(typeof(ExecuteDeleteCommand));
              Bind(typeof(IGetCategoruQuery)).To(typeof(ExecuteGetCategoryQuery));
        }
    }
}
