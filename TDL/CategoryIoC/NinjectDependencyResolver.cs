﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace CategoryIoC
{
    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel kernel;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="kernel">kernel</param>
        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this.kernel = kernel;
        }

        /// <summary>
        /// Starts a resolution scope.
        /// </summary>
        /// <returns></returns>
        public IDependencyScope BeginScope()
        {

            return new NinjectDependencyScope(kernel.BeginBlock());
        }
    }
}
