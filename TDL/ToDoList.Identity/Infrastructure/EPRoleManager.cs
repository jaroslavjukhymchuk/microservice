﻿using ToDoList.Identity.Models.IDS;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using ToDoList.Identity.Models.Roles;

namespace ToDoList.Identity.Infrastructure
{
    public class EPRoleManager : RoleManager<Role, Int32>, IDisposable
    {
        public EPRoleManager(RoleStore<Role, Int32, UserRoleInt32Pk> store) : base(store)
        { }

        public static EPRoleManager Create(IdentityFactoryOptions<EPRoleManager> options, IOwinContext context)
        {
            return new EPRoleManager(new RoleStore<Role, Int32, UserRoleInt32Pk>(context.Get<AppIdentityDbContext>()));
        }
    }
}
