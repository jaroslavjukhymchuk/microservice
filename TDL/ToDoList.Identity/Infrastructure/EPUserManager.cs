﻿using System.Threading.Tasks;
using ToDoList.Identity.Infrastructure.Validators;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using ToDoList.Identity.Models.IDS;
using Microsoft.AspNet.Identity.EntityFramework;
using ToDoList.Identity.Models.Users;
using Incoding.Data;

namespace ToDoList.Identity.Infrastructure
{
    public class UserManager : UserManager<User, Int32>
    {
        public UserManager(IUserStore<User, Int32> store) : base(store)
        { }

        public static UserManager Create(IdentityFactoryOptions<UserManager> options, IOwinContext context)
        {
            AppIdentityDbContext db = context.Get<AppIdentityDbContext>();
            UserManager manager = new UserManager(new UserStoreInt32Pk(db))
            {
                PasswordValidator = new CustomPasswordValidator
                {
                    RequiredLength = 6,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = true,
                    RequireUppercase = false
                }
            };

            manager.UserValidator = new CustomUserValidator(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = true
            };
            var dataProtectionProvider = options.DataProtectionProvider;
           
            return manager;
        }

        public override Task<User> FindAsync(UserLoginInfo login)
        {
            return base.FindAsync(login);
        }

        public async Task<IdentityResult> Createc(User user, string password)
        {
            return await base.CreateAsync(user,password);
        }
        public override Task<User> FindByIdAsync(Int32 userId)
        {
            return base.FindByIdAsync(userId);
        }
      
    }
}
