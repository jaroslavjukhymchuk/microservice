﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoList.Identity.Models.Roles
{
    public class RoleModificationModel
    {
        [Required]
        public string RoleName { get; set; }

        public Int32[] IdsToAdd { get; set; }

        public Int32[] IdsToDelete { get; set; }
    }
}
