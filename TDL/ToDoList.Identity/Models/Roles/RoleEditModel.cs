﻿using System.Collections.Generic;
using ToDoList.Identity.Models.Users;

namespace ToDoList.Identity.Models.Roles
{
    public class RoleEditModel
    {
        public Role Role { get; set; }

        public IEnumerable<User> Members { get; set; }

        public IEnumerable<User> NonMembers { get; set; }
    }
}
