﻿using ToDoList.Identity.Infrastructure;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using ToDoList.Identity.Models.Roles;

namespace ToDoList.Identity.Models.IDS
{
    public class RoleStoreInt32Pk : RoleStore<Role, Int32, UserRoleInt32Pk>
    {
        public RoleStoreInt32Pk(AppIdentityDbContext context)
            : base(context)
        {
        }
    }
}
