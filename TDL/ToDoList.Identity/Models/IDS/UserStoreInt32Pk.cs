﻿using ToDoList.Identity.Infrastructure;
using ToDoList.Identity.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using ToDoList.Identity.Models.Roles;
using ToDoList.Identity.Models.Users;

namespace ToDoList.Identity.Models.IDS
{
    public class UserStoreInt32Pk : UserStore<User, Role, Int32,
        UserLoginInt32Pk, UserRoleInt32Pk, UserClaimInt32Pk>
    {
        public UserStoreInt32Pk(AppIdentityDbContext context)
            : base(context)
        {
        }
    }
}
