﻿using CategoryPersistences.Entities;
using Incoding.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryQueries
{
    public class GetCategoryQuery : QueryBase<List<GetCategoryQuery.Response>>
    {
        #region Properties

        public string Keyword { get; set; }

        #endregion

        #region Nested Classes

        public class Response
        {
            #region Properties

            public string Name { get; set; }

            public string Id { get; set; }

            public Guid IdUser { get; set; }

            #endregion
        }

        #endregion

        protected override List<Response> ExecuteResult()
        {
            return Repository.Query<Category>().Select(category => new Response
            {
                Id = category.Id,
                Name = category.Name,
                IdUser = category.IdUser
            }).ToList();
        }
    }
}
