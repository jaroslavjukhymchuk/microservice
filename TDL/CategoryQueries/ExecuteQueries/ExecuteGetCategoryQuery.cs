﻿using CategoryInterfaces.Dtos;
using CategoryInterfaces.Queries;
using CategoryQueries.Models;
using Incoding.Block.IoC;
using Incoding.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryQueries.ExecuteQueries
{
    public class ExecuteGetCategoryQuery : IGetCategoruQuery
    {
        public IEnumerable<ICategoryDto> GetCategories()
        {

          var dispatcher = IoCFactory.Instance.TryResolve<IDispatcher>();
            return Swap(dispatcher.Query(new GetCategoryQuery()));
        }

        private ICategoryDto Swap(GetCategoryQuery.Response category)
        {
            return new CategoryModel
            {
                Name = category.Name,
                Id = category.Id,
                IdUser = category.IdUser
            };
        }

        private  IEnumerable<ICategoryDto> Swap(List<GetCategoryQuery.Response> categories)
        {
            return categories.Select(category => Swap(category));

        }
    }
}
