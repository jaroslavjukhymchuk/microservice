﻿using CategoryInterfaces;
using Incoding.Block.IoC;
using Incoding.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryCommands.ExecuteCommands
{
    public class ExecuteDeleteCommand : IDeleteCommand
    {
        public void Delete(string id)
        {
            var dispatcher = IoCFactory.Instance.TryResolve<IDispatcher>();
            dispatcher.Push(new DeleteCategoryCommand
            {
                Id = id
            });
        }
    }
}
