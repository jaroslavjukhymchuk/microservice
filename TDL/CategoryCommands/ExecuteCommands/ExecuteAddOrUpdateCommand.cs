﻿using CategoryInterfaces.Commands;
using CategoryInterfaces.Dtos;
using Incoding.Block.IoC;
using Incoding.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryCommands.ExecuteCommands
{
    public class ExecuteAddOrUpdateCommand : IAddOrUpdateCommand
    {
        public void AddOrUpdate(ICategoryDto category)
        {
            var dispatcher = IoCFactory.Instance.TryResolve<IDispatcher>();
            dispatcher.Push(new AddOrEditCategoryCommand
            {
                Name = category.Name,
                Id = category.Id,
                IdUser = category.IdUser
            });
        }
    }
}
