﻿using CategoryPersistences.Entities;
using Incoding.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryCommands
{
    public class DeleteCategoryCommand : CommandBase
    {
        #region Properties

        public string Id { get; set; }

        #endregion

        public override void Execute()
        {
            Repository.Delete<Category>(Id);
        }
    }
}
