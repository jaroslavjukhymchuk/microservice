﻿using CategoryPersistences.Entities;
using FluentValidation;
using Incoding.CQRS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryCommands
{
    public class AddOrEditCategoryCommand : CommandBase
    {
        #region Properties

        public  string Name { get; set; }

        public string Id { get; set; }

        public Guid IdUser { get; set; }

        #endregion

        public override void Execute()
        {
          
           var category = Repository.GetById<Category>(Id) ?? new Category();
           category.Name = Name;
           category.IdUser = IdUser;

           Repository.SaveOrUpdate(category);
          

        }


        #region Nested Classes

        public class Validator : AbstractValidator<AddOrEditCategoryCommand>
        {
            #region Constructors

            public Validator()
            {
                RuleFor(r => r.Name).NotEmpty();
                RuleFor(r => r.IdUser).NotEmpty();
            }

            #endregion
        }

        #endregion
    }
}
