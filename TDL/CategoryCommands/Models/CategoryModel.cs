﻿using CategoryInterfaces.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryCommands.Models
{
    public class CategoryModel : ICategoryDto
    {
        public  string Name { get; set; }

        public string Id { get; set; }

        public Guid IdUser { get; set; }
    }
}
