﻿
using CategoryIoC;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace CategoryApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            CategoryQueries.Bootstrapper.Start();
            CategoryCommands.Bootstrapper.Start();
            CategoryPersistences.Bootstrapper.Start();
            NinjectModule categoryNinjectModule = new CategoryNinjectModule();

            var kernel = new StandardKernel(categoryNinjectModule);
            var ninjectResolver = new NinjectDependencyResolver(kernel);

            // DependencyResolver.SetResolver(ninjectResolver); // MVC
            GlobalConfiguration.Configuration.DependencyResolver = ninjectResolver; // Web API
        }
    }
}
