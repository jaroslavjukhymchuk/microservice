﻿using CategoryApi.Models;
using CategoryInterfaces;
using CategoryInterfaces.Commands;
using CategoryInterfaces.Dtos;
using CategoryInterfaces.Queries;
using CategoryQueries.ExecuteQueries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CategoryApi.Controllers
{
    public class CategoryController : ApiController
    {
        private readonly IGetCategoruQuery _categoryQyery;
        private readonly IDeleteCommand _categoryDeleteCommand;
        private readonly IAddOrUpdateCommand _categoryAddOrUpdateCommand;


        /// <summary>
        /// constructor without param
        /// </summary>
        public CategoryController()
        {
        }

        /// <summary>
        /// constructor for initializing ninject
        /// </summary>
        /// <param name="categoryService">interface service category</param>
        public CategoryController(IGetCategoruQuery categoryQyery, IDeleteCommand categoryDeleteCommand, IAddOrUpdateCommand categoryAddOrUpdateCommand)
        {

            _categoryQyery = categoryQyery;
            _categoryDeleteCommand = categoryDeleteCommand;
            _categoryAddOrUpdateCommand = categoryAddOrUpdateCommand;
        }
        // GET: api/Category
        public IEnumerable<CategoryModel> Get()
        {
            return Swap(_categoryQyery.GetCategories());
        }
        
        public void Post([FromBody]CategoryModel category)
        {
            _categoryAddOrUpdateCommand.AddOrUpdate(category);
        }

        public void Delete(string id)
        {
            _categoryDeleteCommand.Delete(id);
        }

        private CategoryModel Swap(ICategoryDto category)
        {
            return new CategoryModel
            {
                Name = category.Name,
                Id = category.Id,
                IdUser = category.IdUser
            };
        }

        private IEnumerable<CategoryModel> Swap(IEnumerable<ICategoryDto> categories)
        {
            return categories.Select(category => Swap(category));

        }
    }
}
